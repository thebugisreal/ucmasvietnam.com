﻿//option slick slider
$("#slider").slick({
	autoplay: false,
	autoplaySpeed: 2000,
	appendArrows: $("#sliderArrow"),
	prevArrow: '<button type="button" class="slick-prev"><img src="/themes/theme01/assets/css/images/previous.png" alt="Prev"></button>',
	nextArrow: '<button type="button" class="slick-next"><img src="/themes/theme01/assets/css/images/next.png" alt="Next"></button>',
	mobileFirst: true,
	responsive: [{
		breakpoint: 700,
		settings: {
			arrows: true
		}
	},
	{
		breakpoint: 300,
		settings: {
			arrows: false
		}
	}]
});

$("#hot").slick({
	autoplay: false,
	arrows: false,
	mobileFirst:true,
	responsive: [{
		breakpoint: 1200,
		settings: {
			slidesToShow: 3
		}
	}, {
		breakpoint: 700,
		settings: {
			slidesToShow: 2
		}
	}, {
		breakpoint: 300,
		settings: {
			slidesToShow: 1
		}
	}]
});

$("#coach").slick({
	autoplay: false,
	arrows: false,
	dots: true,
	mobileFirst: true,
	appendDots: $("#coachDots"),
	responsive: [{
		breakpoint: 1200,
		settings: {
			slidesToShow: 4
		}
	}, {
		breakpoint: 700,
		settings: {
			slidesToShow: 3
		}
	}, {
		breakpoint: 300,
		settings: {
			slidesToShow: 1
		}
	}]
});

$("#feel").slick({
	autoplay: false,
	arrows: false,
	dots: true,
	mobileFirst: true,
	appendDots: $("#feelDots"),
	responsive: [{
		breakpoint: 1200,
		settings: {
			slidesToShow: 2
		}
	}, {
		breakpoint: 300,
		settings: {
			slidesToShow: 1
		}
	}]
});

$("#news").slick({
	autoplay: false,
	arrows: false,
	mobileFirst: true,
	responsive: [{
		breakpoint: 1200,
		settings: {
			slidesToShow: 4
		}
	}, {
		breakpoint: 700,
		settings: {
			slidesToShow: 3
		}
	}, {
		breakpoint: 300,
		settings: {
			slidesToShow: 1
		}
	}]
});

$("#brand").slick({
	autoplay: false,
	arrows: false,
	mobileFirst: true,
	responsive: [{
		breakpoint: 1200,
		settings: {
			slidesToShow: 8
		}
	}, {
		breakpoint: 700,
		settings: {
			slidesToShow: 5
		}
	}, {
		breakpoint: 300,
		settings: {
			slidesToShow: 2
		}
	}]
});

$("#images").slick({
	autoplay: false,
	arrows: false,
	mobileFirst: true,
	responsive: [{
		breakpoint: 1200,
		settings: {
			slidesToShow: 5
		}
	}, {
		breakpoint: 700,
		settings: {
			slidesToShow: 2
		}
	}, {
		breakpoint: 300,
		settings: {
			slidesToShow: 1
		}
	}]
});

//menu mobile
$("#showMenu").click(function () {
	$("#menuNav").css({
		'left': '0',
		'opacity': '1'
	});
	$("#overlay").css({
		'opacity': '1',
		'visibility': 'initial'
	});
})

$("#closeMenu, #overlay").click(function () {
	$("#menuNav").css({
		'left': '-340px',
		'opacity': '0'
	});
	$("#overlay").css({
		'opacity': '0',
		'visibility': 'hidden'
	});
})

//scroll to top
$(document).ready(function() {
	// declare variable
	var scrollTop = $("#scrollTop");

	$(window).scroll(function() {
		// declare variable
		var topPos = $(this).scrollTop();

		// if user scrolls down - show scroll to top button
		if (topPos > 1000) {
			$(scrollTop).css("opacity", "1");

		} else {
			$(scrollTop).css("opacity", "0");
		}

	}); // scroll END

	//Click event to scroll to top
	$(scrollTop).click(function() {
		$('html, body').animate({
			scrollTop: 0
		}, 800);
		return false;

	}); // click() scroll top EMD
})

//sal.js
sal();